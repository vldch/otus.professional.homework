﻿using homework_01.Context;
using homework_01.DAL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace homework_01
{
    public class Program
    {
        private static User _user = null;
        static void Main(string[] args)
        {         
            using (ApplicationContext db = new ApplicationContext())
            {
                for (int i = 0; i < 5; i++)
                {
                    var user = db.Users.Include(x=>x.Contact).ToList().ElementAt(i);
                    Console.WriteLine("Имя:{0} и фамилия:{1} в базе данных! Контактные данные: номер телефона:{2}, email:{3}!", user.Name, user.LastName,user.Contact.PhoneNumber,user.Contact.Email);
                }
                Console.Write("Введите ваш email:");
                var email = Console.ReadLine();
                _user = db.Users.FirstOrDefault(x => x.Contact.Email == email);
                CreateUserIfNotExist(db, email);
                Console.ReadKey();
            }
        }

        private static void CreateUserIfNotExist(ApplicationContext db, string email)
        {
            if (_user == null)
            {
                Console.Write("Введите ваше имя:");
                var newName = Console.ReadLine();
                Console.Write("Введите вашу фамилию:");
                var newLastName = Console.ReadLine();
                Console.Write("Введите ваш номер телефона:");
                var newPhoneNumber = Console.ReadLine();
                Console.Write("Введите ваш почтовый код:");
                var newPostCode = Console.ReadLine();
                Console.Write("Введите ваш город проживания:");
                var cityConsol = Console.ReadLine();
                var city = db.Cities.FirstOrDefault(x => x.Name == cityConsol);
                Console.Write("Введите вашу страну проживания:");
                var countryConsol = Console.ReadLine();
                var country = db.Countries.FirstOrDefault(x => x.Name == countryConsol);
                Console.Write("Введите вашу улицу проживания:");
                var streetConsol = Console.ReadLine();
                _user = new User()
                {
                    Name = newName,
                    LastName = newLastName,
                    Contact = new Contact()
                    {
                        PhoneNumber = newPhoneNumber,
                        PostCode = newPostCode,
                        City = city != null ? city : new City() { Name = cityConsol },
                        Country = country != null ? country : new Country { Name = countryConsol },
                        Email = email,
                        StreetName = streetConsol
                    }
                };
                db.Users.Add(_user);
                db.SaveChanges();
                Console.WriteLine("Вы внесли имя:{0} и фамилия:{1} в базу данных и контактные данные: номер телефона:{2}, email:{3}!", _user.Name, _user.LastName, _user.Contact.PhoneNumber, _user.Contact.Email);
            }
        }
    }
}
