﻿using homework_01.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework_01.Context
{
    public class DBInitialize
    {
        public static void Initialize(ApplicationContext context)
        {
            if (context == null) return;

            if (context.Users.Any())
            {
                return;
            }

            for (int i = 1; i < 6; i++)
            {
                User user = new User
                {
                    Name = "Name_" + i,
                    LastName = "LastName_" + i,
                    Contact = new Contact()
                    {
                        PhoneNumber = "1234567" + 88 + i,
                        PostCode = "4100" + 73 + i,
                        Email = "name" + i + "@yandex.ru",
                        StreetName = "Moskovskaya " + 9 + i,
                        City = new City() { Name = "City " + i},
                        Country = new Country() { Name = "Country " + i }
                    }
                };
                context.Users.Add(user);
            }
            context.SaveChanges();
        }
    }
}
