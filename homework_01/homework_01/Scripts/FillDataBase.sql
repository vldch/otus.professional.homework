﻿-- Cities
delete from public."Cities" where Id between 1 and 5;
INSERT INTO public."Cities" (Id, "Name") VALUES(1, 'City 1');
INSERT INTO public."Cities" (Id, "Name") VALUES(2, 'City 2');
INSERT INTO public."Cities" (Id, "Name") VALUES(3, 'City 3');
INSERT INTO public."Cities" (Id, "Name") VALUES(4, 'City 4');
INSERT INTO public."Cities" (Id, "Name") VALUES(5, 'City 5');

-- Countries
delete from public."Countries" where Id between 1 and 5;
INSERT INTO public."Countries" (Id, "Name") VALUES(1, 'Country 1');
INSERT INTO public."Countries" (Id, "Name") VALUES(2, 'Country 2');
INSERT INTO public."Countries" (Id, "Name") VALUES(3, 'Country 3');
INSERT INTO public."Countries" (Id, "Name") VALUES(4, 'Country 4');
INSERT INTO public."Countries" (Id, "Name") VALUES(5, 'Country 5');

-- Contacts
delete from public."Contacts" where "Id" between 1 and 5;
INSERT INTO public."Contacts" (Id,"CityId", "CountryId","PhoneNumber","Email","StreetName","PostCode") VALUES(1,1, 1,'123456789','name1@yandex.ru','Moskovskaya 10','410074');
INSERT INTO public."Contacts" (Id,"CityId", "CountryId","PhoneNumber","Email","StreetName","PostCode") VALUES(2,2, 2,'123456790','name2@yandex.ru','Moskovskaya 11','410075');
INSERT INTO public."Contacts" (Id,"CityId", "CountryId","PhoneNumber","Email","StreetName","PostCode") VALUES(3,3, 3,'123456791','name3@yandex.ru','Moskovskaya 12','410076');
INSERT INTO public."Contacts" (Id,"CityId", "CountryId","PhoneNumber","Email","StreetName","PostCode") VALUES(4,4, 4,'123456792','name4@yandex.ru','Moskovskaya 13','410077');
INSERT INTO public."Contacts" (Id,"CityId", "CountryId","PhoneNumber","Email","StreetName","PostCode") VALUES(5,5, 5,'123456793','name5@yandex.ru','Moskovskaya 14','410078');

-- Users
delete from public."Users" where "UserId" between 1 and 5;
INSERT INTO public."Users" (Id,"Name","LastName", "ContactId") VALUES(1,'Name_1','LastName_1','1');
INSERT INTO public."Users" (Id,"Name","LastName", "ContactId") VALUES(2,'Name_2','LastName_2','2');
INSERT INTO public."Users" (Id,"Name","LastName", "ContactId") VALUES(3,'Name_3','LastName_3','3');
INSERT INTO public."Users" (Id,"Name","LastName", "ContactId") VALUES(4,'Name_4','LastName_4','4');
INSERT INTO public."Users" (Id,"Name","LastName", "ContactId") VALUES(5,'Name_5','LastName_5','5');