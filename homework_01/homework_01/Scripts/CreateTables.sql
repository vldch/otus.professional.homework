﻿CREATE TABLE IF NOT EXISTS public."Cities"
(
    "Id" bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "Name" text COLLATE pg_catalog."default",
    CONSTRAINT "PK_Cities" PRIMARY KEY ("Id")
);

CREATE TABLE IF NOT EXISTS public."Countries"
(
    "Id" bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "Name" text COLLATE pg_catalog."default",
    CONSTRAINT "PK_Countries" PRIMARY KEY ("Id")
);

CREATE TABLE IF NOT EXISTS public."Contacts"
(
    "Id" bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "PhoneNumber" text COLLATE pg_catalog."default",
    "Email" text COLLATE pg_catalog."default",
    "StreetName" text COLLATE pg_catalog."default",
    "PostCode" text COLLATE pg_catalog."default",
    "CityId" bigint,
    "CountryId" bigint,
    CONSTRAINT "PK_Contacts" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_Contacts_Cities_CityId" FOREIGN KEY ("CityId")
        REFERENCES public."Cities" ("Id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT,
    CONSTRAINT "FK_Contacts_Countries_CountryId" FOREIGN KEY ("CountryId")
        REFERENCES public."Countries" ("Id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT
);

CREATE INDEX IF NOT EXISTS "IX_Contacts_CityId"
    ON public."Contacts" USING btree
    ("CityId" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS "IX_Contacts_CountryId"
    ON public."Contacts" USING btree
    ("CountryId" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE IF NOT EXISTS public."Users"
(
    "Id" bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "Name" text COLLATE pg_catalog."default",
    "LastName" text COLLATE pg_catalog."default",
    "ContactId" bigint,
    CONSTRAINT "PK_Users" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_Users_Contacts_ContactId" FOREIGN KEY ("ContactId")
        REFERENCES public."Contacts" ("Id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT
);

CREATE INDEX IF NOT EXISTS "IX_Users_ContactId"
    ON public."Users" USING btree
    ("ContactId" ASC NULLS LAST)
    TABLESPACE pg_default;